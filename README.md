
## About Project : To create a project in laravel which will return json formatted value by using REST API-Laravel 8 and Mysql.

- To fetch first 40(Fifty) posts from the endpoint: https://jsonplaceholder.typicode.com/posts
- To Save the fetched posts(from no. 2) into a database table. 
- To Ignore any subsequent requests so that it does not save any data after the first request is served.

- To fetch first 31(thirty one) comments from the endpoint: https://jsonplaceholder.typicode.com/comments 
- To save the fetched comments in a table: 

- Now, to create a REST API which has just an endpoint. 
- The endpoint should accept only json and return the posts which have only one or zero(0) comment. 
- With each post, their respective comments must be present in the returned data.
- The returned data are in json format.


## Installation

- composer create-project laravel/laravel example-app
- Controller:  php artisan make:resource MainController
- Model: php artisan make:model Post
- Model: php artisan make:model Comment
- php artisan make:migration create_posts_table
- Configure posts table staffs
- php artisan make:migration create_comments_table
- Configure comments table staffs

## Database Configuration

- DB_CONNECTION=mysql
- DB_HOST=127.0.0.1
- DB_PORT=3306
- DB_DATABASE=laravel_json
- DB_USERNAME=root
- DB_PASSWORD=

- php artisan migrate
- php artisan serve

Please browse http://localhost:8000/.

- For Storing Posts data: https://jsonplaceholder.typicode.com/posts, Please browse http://localhost:8000/fetch-posts
- Screenshot: /screenshorts/laravel_json_posts_fetch.png

- For storing Comments data: https://jsonplaceholder.typicode.com/comments , Please browse http://localhost:8000/fetch-comments
- Screenshot: /screenshorts/laravel_json_comments_fetch.png

- For Checking the posts which have only one or zero(0) comment and comments With each post in json format, Please browse http://localhost:8000/posts-data
- Screenshot: /screenshorts/laravel_json_posts_with_comment_1_0_fetch.png

You have successfully installed the project.