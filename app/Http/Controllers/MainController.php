<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Comment;
use Illuminate\Support\Facades\Http;

class MainController extends Controller
{
    public function fetchPosts()
    {
        //Featch Data
        $posts = Http::get('https://jsonplaceholder.typicode.com/posts');
        $posts = $posts->json();

        //Limit 40
        $posts = collect($posts)->take(40)->all();

        //From no 2
        unset($posts[0]);

        //Check if already executed
        $dataExist = Post::first();

        //Save into Database
        if(empty($dataExist)) {
            Post::insert($posts);

            return 'Great! Successfully store posts into database';
        } else {
            return 'Already stored posts into database';
        }
    }

    public function fetchComments()
    {
        //Featch Data
        $comments = Http::get('https://jsonplaceholder.typicode.com/comments');
        $comments = $comments->json();

        //Limit 31
        $comments = collect($comments)->take(31)->all();

        //Check if already executed
        $dataExist = Comment::first();

        //Save into Database
        if(empty($dataExist)) {
            Comment::insert($comments);

            return 'Great! Successfully store comments into database';
        } else {
            return 'Already stored comments into database';
        }
    }

    public function postsData()
    {
        $posts = Post::withCount('comments')->get();
        $posts = $posts->where('comments_count', '<=', 1)->all();

        $data = [];
        foreach($posts as $post) {
            $postData = $post->toArray();
            $postData['comments'] = $post->comments->toArray();
            $data[] = $postData;
        }

        return response()->json($data);
    }



}